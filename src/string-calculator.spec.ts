import { BadRequestException } from "@nestjs/common";
import { StringCalculator } from "./string-calculator"

describe('StringCalculator', ()=> {
    describe('Add', ()=> {

        let value = new StringCalculator;

        it('takes an empty string and returns 0', ()=> {
            let result = value.Add('')
            expect(result).toEqual(0)
        })
        it('takes a number and returns it', ()=> {
            let result = value.Add('1')
            expect(result).toEqual(1)
        })
        it('takes two numbers and returns the sum', ()=> {
            let result = value.Add('1,2')
            expect(result).toEqual(3)
        })
        it('takes unknown quantity of numbers and returns the sum', ()=> {
            let result = value.Add('1,2,4,5,6,3,4')
            expect(result).toEqual(25)
        })
        it('numbers can be separated by either comma or new line', ()=> {
            let result = value.Add('1\n2,3')
            expect(result).toEqual(6)
        })
        it('support for different delimiters', ()=> {
            let result = value.Add('//;\n1;2')
            expect(result).toEqual(3)
        })
        it('throws exception on negative numbers and shows all of them in the error message', ()=> {
            try {
                let result = value.Add('//;\n-1;2;56;3;-3;1')
            } catch (e) {
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('negatives not allowed: -1,-3')
            }
        })
        it('ignores numbers bigger than 1000', ()=> {
            let result = value.Add('//;\n1000;2')
            expect(result).toEqual(2)
        })
        it('delimiters can be of any length given the format //[delimiter]\\n', ()=> {
            let result = value.Add('//[***]\n1***2***3')
            expect(result).toEqual(6)
        })
        it('allow multiple delimiters given the format //[delim1][delim2]\\n', ()=> {
            let result = value.Add('//[*][%]\n1*2%3')
            expect(result).toEqual(6)
        })
        it('allow multiple delimiters with length longer than one char', ()=> {
            let result = value.Add('//[***][%%%]\n1***2%%%3')
            expect(result).toEqual(6)
        })
    })
})