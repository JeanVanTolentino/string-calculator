import { BadRequestException } from "@nestjs/common"

export class StringCalculator {
    Add(numbers:string){

        let delimiter = []

        if(numbers.match(/\/\/\[(.*)\]\n/)){
            let match = numbers.match(/\/\/\[(.*)\]\n/)
            delimiter = match[0].slice(3,match[0].length-2).split('][')
            delimiter = delimiter.map(a=> {return `[${a}]`})
            delimiter.unshift(match[0])
            
        } else if (numbers.match(/\/\/(.*)\n/)){
            delimiter = numbers.match(/\/\/(.*)\n/)
            delimiter[1] = `[${delimiter[1]}]`
        }

        if(delimiter.length>0){
            numbers=numbers.replace(delimiter[0],'')
            console.log(numbers)

            for(let i=1; delimiter.length>i; i++){
                let separator = new RegExp(delimiter[i],"g")
                numbers = numbers.replace(separator, ',')
            }
        }

        numbers = numbers.replace(/\n/g, ',')
        let values = numbers.split(',')
        let converted = values.map(a=>{return Number(a)})
        let lessThanThousand = converted.filter(a=> {return a<1000})
        let negatives = converted.filter(a=>{return a<0})

        if(negatives.length>0){
            throw new BadRequestException(`negatives not allowed: ${negatives.join(',')}`)
        }

        return lessThanThousand.reduce((a,b)=>{return a+b}, 0)
    }
}